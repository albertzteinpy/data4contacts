# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from contactmanager.forms import *
from appentity.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg




@login_required(login_url='/')
def entity(request):
    context = RequestContext(request)
    forms_list = [{'fieldname':'phone','label':'Teléfono','plhdr':'(xx-xxxxxxx)'},
                  {'fieldname':'email','label':'Email','plhdr':'email@domine.something'},
                  {'fieldname':'url','label':'Sitio Web','plhdr':'http://www.domine'}
    ]
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user

    return render_to_response('forms/entity_form.html',{'lista':forms_list,'myme':myme},context)


''
@login_required(login_url='/')
def myentities(request):
    context = RequestContext(request)
    entities = Entity.objects.filter(user=request.user)
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
    abc = STrg.ascii_uppercase
    palabras = ['Dialogo','Desarrollo','Ingeniería','Papeles','Azul'] 
    return render_to_response('myentities.html',{'entities':entities,'abc':abc,'myme':myme,'words':palabras},context)

@login_required(login_url='/')
def detailentity(request,ide=None):
    context = RequestContext(request)
    try:
        ent = Entity.objects.get(user=request.user,pk=ide)
    except:
        ent = None
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user


    return render_to_response('detailentity.html',{'ent':ent,'myme':myme},context)

@login_required(login_url='/')
def editentity(request,ide=None):
    context = RequestContext(request)
    forms_list = [{'fieldname':'phone','label':'Teléfono','plhdr':'(xx-xxxxxxx)'},
                  {'fieldname':'email','label':'Email','plhdr':'email@domine.something'},
                  {'fieldname':'url','label':'Sitio Web','plhdr':'http://www.domine'}
    ]
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
   #SE TRATA DE UNA EDICION DE DATOS
    ent = Entity.objects.get(pk=ide)
    return render_to_response('forms/edit_entity_form.html',{'lista':forms_list,'ent':ent,'myme':myme},context)




