# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import simplejson
from appcontact.models import Contact


class EntityFile(models.Model):
    type_file = models.CharField(max_length=200,blank=True)
    entity_file = models.FileField(upload_to='entityFiles')
    '''
        status params 
         case 0 incomplete
         case 1 Complete
         case 2 Deleted
    '''

class Entity(models.Model):
    user = models.ForeignKey(User)
    nameac = models.CharField(max_length=200)
    twitteract = models.CharField(max_length=100,blank=True)
    phonesac = models.TextField(blank=True,null=True)
    urlsac = models.TextField(blank=True,null=True)
    extra = models.TextField(blank=True,null=True)
    photoa = models.IntegerField(blank=True,null=True)
    date_create = models.DateTimeField(auto_now=True)
    addressac = models.CharField(max_length=200,null=True,blank=True)
    cityac = models.CharField(max_length=200,null=True,blank=True)
    cpac = models.CharField(max_length=20,null=True,blank=True)
    lonac = models.FloatField(default=0)
    latac = models.FloatField(default=0)
    
    def phones_list(self):
        if self.phonesac:
            listing = simplejson.loads(self.phonesac)
        else:
            listing = None
        return listing
    
    def urls_list(self):
        if self.urlsac:
            serializing = simplejson.loads(self.urlsac)
        else:
            serializing = None
        return serializing

    def photoimg(self):
        photoim = EntityFile.objects.get(pk=self.photoa)
        return photoim

    def email_list(self):
        listing = self.entityemaillist_set.all()
        return listing

class EntitySocialInfo(models.Model):
    entity_id = models.ForeignKey(Entity)
    social_accounts = models.TextField()
    

class EntityEmailList(models.Model):
    entity_id = models.ForeignKey(Entity)
    email = models.EmailField()
    typeofemail = models.CharField(max_length=10,blank=True)


class As_Contact(models.Model):
    asociation = models.ForeignKey(Entity)
    Contacto = models.ForeignKey(Contact)
    date_create = models.DateTimeField(auto_now=True)
 


