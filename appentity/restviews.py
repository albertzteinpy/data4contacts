# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from django.contrib.auth import authenticate,login as auth_login,logout
from contactmanager.forms import *
from appentity.models import *

@login_required(login_url="/")
def checkcontact(request,contacto):
        emailcheck= EmailList.objects.filter(email=contacto)
        if not emailcheck:
            #c = Contact(user=request.user,namec='Untitle')
            #c.save()
            #emailcheck = EmailList(contacto=c,email=contacto)
            #emailcheck.save()
            response={'r':'newc'}
        else:
            response={'r':'checked'}

        return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addphoto(request):
    if request.POST:
        context = RequestContext(request)
        fields = ['entity_file']
        response = {}
        forma = FormCreator()
        modelo = EntityFile
        data = request.POST.copy()
        files = request.FILES
        response['entity_file']=str(files['entity_file'])
        formaldeido = forma.advanced_form_to_model(modelo,fields)
        formaldeido = formaldeido(data,request.FILES)
        if formaldeido.is_valid():
            newphotoc = EntityFile(entity_file=request.FILES['entity_file'])
            newphotoc.save()
            response['pk']=newphotoc.pk
            return render_to_response('pices/euploaded.html',{'data':response},context)
        else:
            return HttpResponse('no se subira una foto para el usuario seleccionado')
    else:
        return HttpResponse()


def delphoto(request,idu):
    context = RequestContext(request)
    if idu:
        p = EntityFile.objects.get(pk=idu).delete()
    response = {'del':True}
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addentity(request):
    return HttpResponse('') 

@login_required(login_url="/")
def myentities(request):
    return HttpResponse('') 


@login_required(login_url="/")
def addentity(request):
    response = {}
    fields = ['user','nameac','addressac','cpac','twitteract','extra','phonesac','urlsac','photoa']
    forma = FormCreator()
    modelo = Entity
    data = request.POST.copy()
    data['user']=request.user.pk
    phones = request.POST.getlist('phone')
    phonestypes = request.POST.getlist('typeofphone')
    urls = request.POST.getlist('url')
    urlstypes = request.POST.getlist('typeofurl')
    phoneList = [
        {'phone':x,
        'typePhone':phonestypes[phones.index(x)],
        } for x in phones]
    urlList = [
        {'url':x,
        'typeofUrl':urlstypes[urls.index(x)],
        } for x in urls]

    phonesJson = simplejson.dumps(phoneList)
    urlsJson = simplejson.dumps(urlList)
    data['phonesac']=phonesJson
    data['urlsac'] = urlsJson
    if data['id']:
        instanced = Entity.objects.get(pk=data['id'])
    else:
        instanced = None


    formaldeido = forma.advanced_form_to_model(modelo,fields)
    formaldeido = formaldeido(data,instance=instanced)
    if formaldeido.is_valid():
        datar = formaldeido.save()
        if datar.pk:
            formater = FormatingData()
            emails = request.POST.getlist('email',None)
            typeofemails = request.POST.getlist('typeofemail')
            params = [emails,typeofemails]
            rup = formater.saveListEmaiLEntity(params,datar)



            response['data']=data 
            response['saved']=True
            response['pk']=datar.pk
            return HttpResponse(simplejson.dumps(response))
    else:
        response['saved']='faild'
        response['err']=formaldeido.errors
        return HttpResponse(simplejson.dumps(response))

    return HttpResponse(simplejson.dumps(response))


