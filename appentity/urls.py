from django.conf.urls.defaults import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('appentity.views',
    url(r'^entity','entity',name='entity'),
    url(r'^myentities','myentities',name='myentities'),
    url(r'^entity','entity',name='entity'),
    url(r'^detailentity/(?P<ide>\d+)/$','detailentity',name='detailentity'),
    url(r'^editentity/(?P<ide>\d+)/$','editentity',name='editentity'),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns += patterns('appentity.restviews',
    url(r'^addphotoentity$','addphoto',name='addphoto'),
    url(r'^edelphoto/(?P<idu>\d+)/$','delphoto',name='delphoto'),
    url(r'^addentity','addentity',name='addentity'),
 )
