# -*- encoding: utf-8 -*-

from django.forms import ModelForm
import simplejson
from appcontact.models import *
from appentity.models import *
class FormCreator:
    def __init__(self):
        pass

    def form_to_model(self,modelo):
        if modelo: 
            meta=type('Meta',(),{'model':modelo})
            crearForm=type('modelform',(ModelForm,),{"Meta":meta})
        return crearForm

    def advanced_form_to_model(self,modelo,fields):
        meta = type('Meta',(),{"model":modelo,'fields':fields})
        KtemodelfomrKlss = type('modelform',(ModelForm,),{"Meta":meta})
        return KtemodelfomrKlss


class FormatingData:
    def __init__(self):
        pass

    def list_params(self,params):
        myname = params[0]
        myvalues = params[1]
        typesof = params[2]
        listing = []
        for x in myvalues:
            if x:
                listing.append({myname:x,
                'type'+str(myname):typesof[myvalues.index(x)],
                })

        return simplejson.dumps(listing)

    def saveListRelated(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EmailList.objects.filter(contacto=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EmailList(contacto=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing

    def saveListEmaiLEntity(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EntityEmailList.objects.filter(entity_id=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EntityEmailList(entity_id=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing


