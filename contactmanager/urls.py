from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    (r'^', include('appcontact.urls')),
    (r'^', include('appentity.urls')),

)
