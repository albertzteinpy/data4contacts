# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from contactmanager.forms import *
from appcontact.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from django.db.models import Q


# ********************************** DRY METHODS    ******************************************************


def abc_tag_list():
    ''' 
        This method need a Tag django-model
    '''
    a = {x:[] for x in STrg.ascii_uppercase}
    T = Tag.objects.all()
    for x in T:
        a['%s'%(x)].append({'tag':'%s'%(x.tagname)})
    return a

# VIEWS TO "THE SISTEMA" ----------------------------------------------------------


def index(request):
    context = RequestContext(request)
    user = request.user
    if user.is_active:
        return redirect('/inicio')
    return render_to_response("login.html",{},context)


@login_required(login_url="/")
def inicio(request):
    context = RequestContext(request)
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
    return render_to_response("inicio.html",{'myme':myme},context)

@login_required(login_url='/')
def contact_form(request):
    context = RequestContext(request)
    forms_list = [{'fieldname':'phone','label':'Teléfono','plhdr':'(xx-xxxxxxx)'},
                  {'fieldname':'email','label':'Email','plhdr':'email@domine.something'},
                  {'fieldname':'url','label':'Sitio Web','plhdr':'http://www.domine'}
    ]
    if request.GET:
    #SE TRATA DE UNA EDICION DE DATOS
        contact = Contact.objects.get(pk=request.GET['idc'])
    else:
        contact = None

    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user


    states=Sepomex.objects.distinct('estado').values('estado','clave_estado')

    return render_to_response('forms/contact_form.html',
                              {'lista':forms_list,
                              'contact':contact,
                              'myme':myme,
                              'states':states},
                              context)

@login_required(login_url='/')
def edtcontact(request,idc=None):
    context = RequestContext(request)
    forms_list = [{'fieldname':'phone','label':'Teléfono','plhdr':'(xx-xxxxxxx)'},
                  {'fieldname':'email','label':'Email','plhdr':'email@domine.something'},
                  {'fieldname':'url','label':'Sitio Web','plhdr':'http://www.domine'}
    ]
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
   #SE TRATA DE UNA EDICION DE DATOS
    contact = Contact.objects.get(pk=idc)
    states=Sepomex.objects.distinct('estado').values('estado','clave_estado')
    return render_to_response('forms/edt_contact.html',{'lista':forms_list,'contact':contact,
                              'myme':myme,'states':states},context)

@login_required(login_url='/')
def delcontact(request,idc=None):
    context = RequestContext(request)
    contact = Contact.objects.get(pk=idc)
    contact.status=0
    contact.save()
    return redirect('/mycontacts')
    return render_to_response('forms/edt_contact.html',{'lista':forms_list,'contact':contact,'myme':myme},context)




@login_required(login_url='/')
def mycontacts(request):
    context = RequestContext(request)
    contacts = Contact.objects.filter(~Q(status=0)).order_by('-date_create')
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
    abc = abc_tag_list()
    return render_to_response('mycontacts.html',{'contacts':contacts,'abc':abc,'myme':myme},context)


# DATAIL CONTACT*******************************************************************************************


@login_required(login_url='/')
def detailcontact(request,idc=None):
    context = RequestContext(request)
    
    try:
        c = Contact.objects.get(usercreator=request.user,pk=idc)
    except:
        c = None

    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user

    return render_to_response('detailcontact.html',{'c':c,'myme':myme},context)

# SEARCH RESULTS *********************************************************************************************
def resultados(request):
    context = RequestContext(request)
    wordss = request.POST.get('words',None)
    kwargs = {'status':1}
    ekwargs = {} #{'status':1}
    args = Q()
    eargs = Q()
    namec = request.POST.get('namec',None)
    

    estado = request.POST.get('estado',None)
    tag = request.POST.get('tag',None)
    if namec:
        kwargs['namec']='%s'%(namec)
    if tag:
        kwargs['tags__icontains']="%s"%(tag)
    formdata = request.POST.copy()
    
    states = Sepomex.objects.all().distinct('estado')
    if estado:
        city = Sepomex.objects.filter(estado=estado).distinct('municipio')
        kwargs['estado']=estado
    else:
        city = None
    
    locations = [states,city]    
    
    if wordss:
        args = Q(namec__icontains=wordss)
        eargs = Q(nameac__icontains=wordss)
    
    response = []
    
    c = Contact.objects.filter(*[args],**kwargs)
    o = Entity.objects.filter(*[eargs],**ekwargs)
    
    for item in c:
        response.append({'name':item.namec,'type':'Contacto','photoimg':item.photoimg,'link':'/detailcontact/'+str(item.pk)+'/',
                         'state':item.estado,'address':item.address,'phones':item.phone_list
                        })
    for item in o:
        response.append({'name':item.nameac,'type':'Organización','photoimg':item.photoimg,'link':'/detailentity/'+str(item.pk)+'/',
                          'address':item.addressac,'phones':item.phones_list
                        })
    abc=abc_tag_list()
    try: 
        myme=Contact.objects.get(userrelated=request.user.pk)
    except:
        myme=request.user

    del formdata['csrfmiddlewaretoken']
    print kwargs
    return render_to_response('myresults.html',
                             {'response':response,
                              'abc':abc,'myme':myme,
                              'formdata':formdata,
                              'location':locations},
                              context)

def logoff(request):
    logout(request)
    return redirect('/')
