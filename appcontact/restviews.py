# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from django.contrib.auth import authenticate,login as auth_login,logout
from contactmanager.forms import *
from appcontact.models import *
from sets import Set


def login(request):
    usr = request.POST.get("usr")
    pwd = request.POST.get("pwd")
    user = authenticate(username=usr, password=pwd)
    if user.is_active:
        auth_login(request,user)
        response = {'l':'ok'}
    else:
        response = {'l':'fail'}
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def checkcontact(request,contacto):
        emailcheck= EmailList.objects.filter(email=contacto)
        if not emailcheck:
            #c = Contact(user=request.user,namec='Untitle')
            #c.save()
            #emailcheck = EmailList(contacto=c,email=contacto)
            #emailcheck.save()
            response={'r':'newc'}
        else:
            response={'r':'checked'}

        return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addcontact(request):
    response = {}
    fields = ['usercreator','namec','lastname','chargec',
              'extrac','phones','urls','photoc','estado','delomun','colonia','address','cp']
    forma = FormCreator()
    formater = FormatingData() 
    modelo = Contact
    data = request.POST.copy()
    data['usercreator']=request.user.pk
    phones_process = ['phones',request.POST.getlist('phone'),request.POST.getlist('typeofphone')]
    url_process = ['urls',request.POST.getlist('url'),request.POST.getlist('typeofurl') ]
    phones = formater.list_params(phones_process)
    urls = formater.list_params(url_process)
    socialinfo = {'socialnetw':'twitter','account':request.POST['twitter']}
    socialinfo = simplejson.dumps(socialinfo)
    data['phones']=phones
    data['urls'] = urls
    if data['id']:
        instanced = Contact.objects.get(pk=data['id'])
    else:
        instanced = None
    formaldeido = forma.advanced_form_to_model(modelo,fields)
    formaldeido = formaldeido(data,instance=instanced)
    if formaldeido.is_valid():
        datar = formaldeido.save()
        if datar.pk:
            emails = request.POST.getlist('email',None)
            typeofemails = request.POST.getlist('typeofemail')
            params = [emails,typeofemails]
            rup = formater.saveListRelated(params,datar)
            try:
                socialsaver = ContactSocialInfo.objects.get(Contact_id=datar).delete() 
            except:
                pass
            socialsaver = ContactSocialInfo.objects.create(Contact_id=datar,social_accounts=socialinfo)
            response['saved']=True
            response['pk']=datar.pk
            return HttpResponse(simplejson.dumps(response))
    else:
        response['saved']='faild'
        response['err']=formaldeido.errors
        return HttpResponse(simplejson.dumps(response))

    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addphoto(request):
    if request.POST:
        context = RequestContext(request)
        fields = ['contact_file','type_file']
        response = {}
        forma = FormCreator()
        modelo = ContactFiles
        data = request.POST.copy()
        data['type_file']='profilePhoto'
        files = request.FILES
        response['contact_file']=str(files['contact_file'])
        formaldeido = forma.advanced_form_to_model(modelo,fields)
        formaldeido = formaldeido(data,request.FILES)
        if formaldeido.is_valid():
            newphotoc = ContactFiles(contact_file=request.FILES['contact_file'],type_file=data['type_file'])
            newphotoc.save()
            response['pk']=newphotoc.pk
            return render_to_response('pices/uploaded.html',{'data':response},context)
        else:
            return HttpResponse(simplejson.dumps(formaldeido.errors))
    else:
        return HttpResponse()


def delphoto(request,idu):
    context = RequestContext(request)
    if idu:
        try: p = ContactFiles.objects.get(pk=idu).delete()
        except: p = None
    response = {'del':True}
    
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addtag(request):
    tag = request.POST.get('tag',None)
    pk = request.POST.get('contactpk',None)
    if pk:
        tagbd,failtag = Tag.objects.get_or_create(tagname=tag)
        c = Contact.objects.get(pk=pk)
        if c.tags:
            current_tag = simplejson.loads(c.tags) 
        else:
            current_tag = []
        current_tag.append(tagbd.tagname)
        current_tag = sorted(set(current_tag))
        c.tags = simplejson.dumps(current_tag)
        c.save()
        texttags = Tag.objects.filter(tagname__in=current_tag).values('tagname')
        texttags = [x['tagname'] for x in texttags]
        tag = simplejson.dumps(texttags)
    else:
        tag = 'FAIL'
    return HttpResponse(tag) 

@login_required(login_url="/")
def myentities(request):
    return HttpResponse('') 


def restlocation(request):
    pk = request.GET.get('pk',None)
    if pk:
        municipio = Sepomex.objects.filter(estado=pk).distinct('municipio').values('clave_municipio','municipio').order_by('municipio')
        municipio = [{'pk':x['clave_municipio'],'municipio':x['municipio']} for x in municipio]
        municipio = simplejson.dumps(municipio)
    else:
        municipio = None
    return HttpResponse(municipio)


def colonia(request):
    pk = request.GET.get('pk',None)
    if pk:
        municipio = Sepomex.objects.filter(municipio=pk).distinct('asentamiento').values('clave_asentamiento','asentamiento').order_by('asentamiento')
        municipio = [{'pk':x['clave_asentamiento'],'municipio':x['asentamiento']} for x in municipio]
        municipio = simplejson.dumps(municipio)
    else:
        municipio = None
    return HttpResponse(municipio)


@login_required(login_url="/")
def addnote(request):
    form = FormCreator()
    modelo = Note
    data = request.POST.copy()
    formaldeido = form.form_to_model(modelo)
    formaldeido= formaldeido(data)
    if formaldeido.is_valid():
        doit = formaldeido.save()
        sendername = Contact.objects.get(userrelated=doit.sender.pk)
        formdata = {'receptor':'%s %s' %(doit.receptor.namec,doit.receptor.lastname),
                    'sender':'%s %s' %(sendername.namec,sendername.lastname),
                    'text':doit.text,
                    'fecha':'%s' %(doit.date_create)
                    }
        response = simplejson.dumps({'id':doit.pk,'formdata':formdata})
        return HttpResponse(response)
    else:
        errors = simplejson.dumps(formaldeido.errors)
        return HttpResponse(errors)
