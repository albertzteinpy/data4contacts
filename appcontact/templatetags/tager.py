# coding: utf-8
from django import template
from django.core.urlresolvers import reverse
from appcontact.models import *
from sets import Set
import string as STrg
register = template.Library()

options = [
            ['Trabajo','Casa','Otro']
]

@register.simple_tag
def alberto(target,selected):
    if str(options[0][target])==str(selected):
        return 'selected="selected"'
    else:
        return ''

@register.simple_tag   
def tagsbyletter():
    a = {x:[] for x in STrg.ascii_uppercase}
    T = Tag.objects.all()
    for x in T:
        a[x.tagname[0]].append({'tag':x.tagname})

    #letter = sorted(set(letter))
    return a



