# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
import simplejson


class ContactFiles(models.Model):
    type_file = models.CharField(max_length=200,blank=True)
    contact_file = models.FileField(upload_to='ContactFiles')
    '''
        status params 
         case 0 incomplete
         case 1 Complete
         case 2 Deleted
    '''
class Contact(models.Model):
    usercreator = models.ForeignKey(User)
    userrelated = models.IntegerField(null=True,blank=True)
    namec = models.CharField(max_length=200,blank=True,null=True)
    lastname = models.CharField(max_length=200,blank=True,null=True)
    chargec = models.CharField(max_length=100,blank=True,null=True)
    phones = models.TextField(blank=True,null=True)
    urls = models.TextField(blank=True,null=True)
    date_create = models.DateTimeField(default=datetime.now)
    photoc = models.IntegerField(blank=True,null=True)
    status = models.CharField(max_length=2,default=1)
    extrac = models.TextField(blank=True,null=True)
    tags = models.TextField(blank=True,null=True) 
    address = models.CharField(max_length=200)
    cp = models.CharField(max_length=10)
    colonia = models.CharField(max_length=200)
    delomun = models.CharField(max_length=200)
    estado = models.CharField(max_length=200)
    lon = models.FloatField(default=0)
    lat = models.FloatField(default=0)


    def photoimg(self):
        photoim = ContactFiles.objects.get(pk=self.photoc)
        return photoim

    def phone_list(self):
        listing = simplejson.loads(self.phones)
        return listing

    def socialinfo(self):
        listando = [x.social_accounts for x in self.contactsocialinfo_set.all()]
        return simplejson.loads(listando[0])

    def urls_list(self):
        if self.urls:
            listado = simplejson.loads(self.urls)
        else:
            listado = None
        return listado

    def tages(self):
        if self.tags:
            tags = simplejson.loads(self.tags)
            listing = tags
        else:
            listing = None
        return listing

class ContactSocialInfo(models.Model):
    Contact_id = models.ForeignKey(Contact)
    social_accounts = models.TextField()
    

class EmailList(models.Model):
    contacto = models.ForeignKey(Contact)
    email = models.EmailField()
    typeofemail = models.CharField(max_length=10,blank=True)


class Tag(models.Model):
    tagname = models.CharField(max_length=200,null=True,default='Untitle')
    def __unicode__(self):
        return '%s' %(self.tagname[0].upper())
    
class Sepomex(models.Model):
    codigo_postal = models.CharField(max_length=5)
    asentamiento = models.CharField(max_length=250)
    tipo_asentamiento = models.CharField(max_length=100)
    municipio = models.CharField(max_length=250)
    estado = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=250)
    descripcion_codigo_postal = models.CharField(max_length=5)
    clave_estado = models.CharField(max_length=2)
    clave_oficina = models.CharField(max_length=5)
    clave_tipo_asentamiento = models.CharField(max_length=5)
    clave_municipio = models.CharField(max_length=3)
    clave_asentamiento = models.CharField(max_length=2)
    zona = models.CharField(max_length=40)
    clave_ciudad = models.CharField(max_length=2)

class Note(models.Model):
    class Meta:
        ordering = ['date_create']
    text = models.TextField(default='untitle')
    sender = models.ForeignKey(User)
    receptor = models.ForeignKey(Contact)
    date_create = models.DateField(auto_now_add=True,default=datetime.now)

