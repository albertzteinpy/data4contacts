function ajax_send_tag()
{
    data = $(this).parents('form:first').serializeArray();
    $.ajax({
            url:'/addtag/',
            data:data,
            type:'post',
            dataType:'json',
            success:function(response){
                $('#tage_boxe').html(' ');
                $.each(response,function(x,y){
                    $('#tage_boxe').append('<p class="inline_blocke t7">'+y+' &nbsp;</p>');
                });
                $('.ligthform').remove();
            }
        });


}



function edipokill()
{
    $(this).parents('.ligthform:first').remove();
}

function tagshow()
{
    $('.ligthform').remove();
    divbox = document.createElement('div');
    divbox.className='ligthform';
    boxform = $('#tagerbox').clone();
    $(boxform).find('.edipokill').click(edipokill);
    $(boxform).attr('id','').removeClass('blind');
    $(divbox).append(boxform);
    $(this).after(divbox);    
    $(boxform).find('.btn_add_tag').click(ajax_send_tag);
    return false;
}


function deleteperson()
{
    if(confirm('para eliminar el registro, clic en OK')) return true;
    else return false;
}


function addnote()
{
    var data=$(this).serializeArray();
    var uri=$(this).attr('action');
    var methodman=$(this).attr('method');
    $.ajax({url:uri,
            data:data,
            type:methodman,
            dataType:'json',
            success:function(response){
                if(response.id)
                {
                    $('[name=text]').val('');
                    var newbox = $('.notes_list:first').clone();
                    $(newbox).find('.fecha_s').html(response.formdata.fecha);
                    $(newbox).find('.sender').html(response.formdata.sender);
                    $(newbox).find('.text').html(response.formdata.text);
                    $('.notes_box').prepend(newbox);
                    


                }
                else{
                    alert(response.errors);   
                }
            }});
    return false;
}


$(document).ready(function(){
    $('.addtag').click(tagshow);
    $('.deleteperson').click(deleteperson);
    $('#addnotes').submit(addnote);
});
